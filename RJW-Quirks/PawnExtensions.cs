﻿using rjw;
using rjwquirks.Modules.Quirks;
using System.Linq;
using Verse;

namespace rjwquirks
{
    public static class PawnExtensions
    {
        public static bool HasQuirk(this Pawn pawn, QuirkDef quirk)
        {
            return xxx.is_human(pawn) && pawn.GetQuirks().Contains(quirk);
        }

        public static QuirkSet GetQuirks(this Pawn pawn)
        {
            return pawn.GetComp<QuirkSet>();
        }
    }
}
