﻿using rjw;
using rjwquirks.Modules.Shared.Events;
using System.Linq;
using Verse;

namespace rjwquirks.Modules.Quirks.EventHandlers
{
    public class QuirkGenerator : RjwEventHandler
	{
		public override void HandleEvent(RjwEvent ev)
		{
			if (!ev.args.TryGetArg(RjwEventArgNames.Pawn, out Pawn pawn))
			{
				ModLog.Error($"QuirkGenerator recieved {ev.def}, but event has no '{RjwEventArgNames.Pawn}' argument");
				return;
			}

			Generate(pawn);
		}

		/// <summary>
		/// Generate quirks for the pawn.
		/// Caution: Does not clears existing quirks
		/// </summary>
		public static void Generate(Pawn pawn)
		{
			if (!pawn.RaceHasSexNeed() || (pawn.kindDef.race.defName.ToLower().Contains("droid") && !AndroidsCompatibility.IsAndroid(pawn)))
			{
				return;
			}

			QuirkSet quirks = pawn.GetQuirks();

			if (pawn.IsAnimal())
			{
				GenerateForAnimal(quirks);
			}
			else
			{
				GenerateForHumanlike(quirks);
			}
		}

		/// <summary>
		/// Rolls an X number between 0 and max quirks setting.
		/// Tries to add random quirks to the QuirkSet until X are added or out of available quirks.
		/// </summary>
		static void GenerateForHumanlike(QuirkSet quirks)
		{
			var count = Rand.RangeInclusive(0, RJWPreferenceSettings.MaxQuirks);
			var availableQuirks = DefDatabase<QuirkDef>.AllDefsListForReading
				.Where(def => def.rarity != QuirkRarity.ForcedOnly) // Have to earn these
				.ToArray(); // ToArray to get a shuffleable copy
			availableQuirks.Shuffle();

			// Some quirks may be hard for a given pawn to indulge in.
			// For example a female homosexual will have a hard time satisfying an impregnation fetish.
			// But rimworld is a weird place and you never know what the pawn will be capable of in the future.
			// We still don't want straight up contradictory results like fertile + infertile.
			foreach (var quirkDef in availableQuirks)
			{
				if (count == 0)
				{
					break;
				}

				if (!quirks.CanBeAdded(quirkDef))
				{
					continue;
				}

				count--;
				quirks.AddQuirk(quirkDef);
			}
		}

		/// <summary>
		/// Original method rolled 10% chance on 3 hardcoded animal quirks.
		/// This implementation takes MaxQuirks (3 by default) number of quirks from
		/// available for animals and rolls 10% chance for each
		/// </summary>
		static void GenerateForAnimal(QuirkSet quirks)
		{
			int count = RJWPreferenceSettings.MaxQuirks;
			var availableQuirks = DefDatabase<QuirkDef>.AllDefsListForReading
				.Where(def => def.rarity != QuirkRarity.ForcedOnly)
				.ToArray();
			availableQuirks.Shuffle();

			foreach (var quirkDef in availableQuirks)
			{
				if (count == 0)
				{
					break;
				}

				if (!quirks.CanBeAdded(quirkDef))
				{
					continue;
				}

				count--;

				if (Rand.Chance(0.1f))
				{
					quirks.AddQuirk(quirkDef);
				}
			}
		}
	}
}
