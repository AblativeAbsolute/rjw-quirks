﻿using rjw;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class LogicalOr : LogicalMultipart
	{
		public override bool SexSatisfies(SexProps sexProps)
		{
			for (int i = 0; i < parts.Count; i++)
			{
				if (parts[i].SexSatisfies(sexProps))
				{
					return true;
				}
			}

			return false;
		}
	}
}
