﻿using rjw;
using System.Collections.Generic;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class LogicalNot : SexSelector
    {
        public ISexSelector negated;

        public override bool SexSatisfies(SexProps sexProps) => !negated.SexSatisfies(sexProps);

        public override void SetParent(QuirkDef quirkDef)
        {
            base.SetParent(quirkDef);
            negated.SetParent(quirkDef);
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (negated == null)
            {
                yield return "<negated> is empty";
            }
            else
            {
                foreach (string error in negated.ConfigErrors())
                {
                    yield return error;
                }

            }
        }
    }
}
