﻿using rjw;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class CanBeImpregnated : SexSelector
    {
        public override bool SexSatisfies(SexProps sexProps) => sexProps.hasPartner() && PregnancyHelper.CanImpregnate(sexProps.partner, sexProps.pawn, sexProps.sexType);
    }
}
