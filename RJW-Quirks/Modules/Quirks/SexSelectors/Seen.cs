﻿using rjw;
using Verse;
using Verse.AI;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class Seen : SexSelector
    {
        // Current implementation works only if somebody sees pawn exactly at the moment quirks are evaluated
        public override bool SexSatisfies(SexProps sexProps) => sexProps.hasPartner() && SexSeen(sexProps);

        public static bool SexSeen(SexProps sexProps)
        {
            bool isZoophile = xxx.is_zoophile(sexProps.pawn);
            return sexProps.pawn.Map.mapPawns.AllPawnsSpawned.Any(x =>
                x != sexProps.pawn
                && x != sexProps.partner
                && !x.Dead
                && (isZoophile || !xxx.is_animal(x))
                && x.CanSee(sexProps.pawn));
        }
    }
}
