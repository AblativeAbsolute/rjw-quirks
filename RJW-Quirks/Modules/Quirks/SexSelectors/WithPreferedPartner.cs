﻿using rjw;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class WithPreferedPartner : SexSelector
    {
        public override bool SexSatisfies(SexProps sexProps) => sexProps.hasPartner() && parentDef.partnerPreference?.PartnerSatisfies(sexProps.pawn, sexProps.partner) == true;
    }
}
