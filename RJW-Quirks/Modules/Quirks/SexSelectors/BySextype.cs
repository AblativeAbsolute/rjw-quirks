﻿using rjw;
using System.Collections.Generic;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class BySextype : SexSelector
	{
		public xxx.rjwSextype sextype = xxx.rjwSextype.None;

		public override bool SexSatisfies(SexProps sexProps) => sexProps.hasPartner() && sexProps.sexType == sextype;

		public override IEnumerable<string> ConfigErrors()
		{
			foreach (string error in base.ConfigErrors())
			{
				yield return error;
			}

			if (sextype == xxx.rjwSextype.None)
			{
				yield return "<sextype> is not filled or has value \"None\"";
			}
		}
	}
}
