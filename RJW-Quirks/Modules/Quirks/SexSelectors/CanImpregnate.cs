﻿using rjw;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public class CanImpregnate : SexSelector
    {
        public override bool SexSatisfies(SexProps sexProps) => sexProps.hasPartner() && PregnancyHelper.CanImpregnate(sexProps.pawn, sexProps.partner, sexProps.sexType);
    }
}
