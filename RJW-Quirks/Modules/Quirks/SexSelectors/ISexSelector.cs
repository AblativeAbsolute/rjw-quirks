﻿using rjw;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public interface ISexSelector : Shared.IDefPart
    {
        void SetParent(QuirkDef quirkDef);
        bool SexSatisfies(SexProps sexProps);
    }
}