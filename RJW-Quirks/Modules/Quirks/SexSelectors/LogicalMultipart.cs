﻿using System.Collections.Generic;

namespace rjwquirks.Modules.Quirks.SexSelectors
{
    public abstract class LogicalMultipart : SexSelector
    {
        public List<ISexSelector> parts = new List<ISexSelector>();

        public override void SetParent(QuirkDef quirkDef)
        {
            base.SetParent(quirkDef);
            parts.ForEach(selector => selector.SetParent(quirkDef));
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (parts.Count < 2)
            {
                yield return "<parts> should have at least 2 elements";
            }

            foreach (var part in parts)
            {
                foreach (string error in part.ConfigErrors())
                {
                    yield return error;
                }
            }
        }
    }
}
