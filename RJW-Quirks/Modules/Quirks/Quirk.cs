﻿using rjwquirks.Modules.Shared.Events;
using System.Text;
using Verse;

namespace rjwquirks.Modules.Quirks
{
    public class Quirk : IExposable
    {
        public QuirkDef def;
        public Pawn pawn;

        /// <summary>
        /// Adjusted for the owner and cached quirk description
        /// </summary>
        public string Description
        {
            get
            {
                if (descriptionCache == null)
                {
                    // Description bulding is a fairly pricy operation
                    descriptionCache = def.GetDescriptionFor(pawn);
                }
                return descriptionCache;
            }
        }

        /// <summary>
        /// Gender specific quirk label
        /// </summary>
        public string Label => def.GetLabelFor(pawn);

        protected string descriptionCache;

        /// <summary>
        /// For save loading only
        /// </summary>
        public Quirk() { }

        public Quirk(QuirkDef def, Pawn pawn)
        {
            this.def = def;
            this.pawn = pawn;
        }

        /// <summary>
        /// Pass RJW event to all comps of this quirk's def
        /// </summary>
        public void NotifyEvent(RjwEvent ev) => def.comps.ForEach(comp => comp.NotifyEvent(ev));

        public string TipString()
        {
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.AppendLine(Label.Colorize(UnityEngine.Color.yellow));
            stringBuilder.Append(Description);
            return stringBuilder.ToString();
        }

        public void ExposeData()
        {
            Scribe_Defs.Look(ref def, "def");

            if (Scribe.mode == LoadSaveMode.ResolvingCrossRefs && def == null)
            {
                def = DefDatabase<QuirkDef>.GetRandom();
            }
        }

        public override string ToString() => $"Quirk({def})";
    }
}
