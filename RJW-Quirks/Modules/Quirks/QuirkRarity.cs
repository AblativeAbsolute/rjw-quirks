﻿namespace rjwquirks.Modules.Quirks
{
    public enum QuirkRarity
    {
        ForcedOnly,
        Common
    }
}
