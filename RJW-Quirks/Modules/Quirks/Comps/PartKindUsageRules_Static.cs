﻿using rjw.Modules.Interactions.Enums;
using rjw.Modules.Interactions.Objects;
using rjw.Modules.Shared;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    public class PartKindUsageRules_Static : PartKindUsageRules
    {
        // rjw.Modules.Shared can't be loaded directly because it uses properties.
        // Probably should change that, but it'll cause more ripples then I'm willing to handle rn
        public class WeightedDef
        {
            public LewdablePartKind partKind;
            public float weightMultiplier;
        }

        public List<WeightedDef> self = new List<WeightedDef>();
        public List<WeightedDef> partner = new List<WeightedDef>();

        public override IEnumerable<Weighted<LewdablePartKind>> GetModifiersForPawn(InteractionPawn quirkOwner, InteractionPawn partner)
        {
            return self.ConvertAll(ruleDef => new Weighted<LewdablePartKind>(ruleDef.weightMultiplier, ruleDef.partKind));
        }

        public override IEnumerable<Weighted<LewdablePartKind>> GetModifiersForPartner(InteractionPawn quirkOwner, InteractionPawn partner)
        {
            return this.partner.ConvertAll(ruleDef => new Weighted<LewdablePartKind>(ruleDef.weightMultiplier, ruleDef.partKind));
        }

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (self.NullOrEmpty() && partner.NullOrEmpty())
            {
                yield return "Both <self> and <partner> can not be empty";
            }
        }
    }
}
