﻿using rjw;

namespace rjwquirks.Modules.Quirks.Comps
{
    public class ThoughtAdder_OnSexEvent_NonPreferred : ThoughtAdder_OnSexEvent
    {
        public override bool ShouldApplyThought(SexProps props) => !parent.IsSatisfiedBySex(props);
    }
}
