﻿using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    public class SexAppraisalModifier_ApplyMultiplier : SexAppraisalModifier
    {
        public float multiplier = 1f;

        public override void ModifyValue(Pawn quirkOwner, Pawn partner, ref float value)
        {
            value *= multiplier;
        }

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (multiplier == 1f)
            {
                yield return "<multiplier> is empty or is 1";
            }
        }
    }
}
