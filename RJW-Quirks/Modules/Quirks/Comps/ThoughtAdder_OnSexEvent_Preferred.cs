﻿using rjw;

namespace rjwquirks.Modules.Quirks.Comps
{
    public class ThoughtAdder_OnSexEvent_Preferred : ThoughtAdder_OnSexEvent
    {
        public override bool ShouldApplyThought(SexProps props) => parent.IsSatisfiedBySex(props);
    }
}
