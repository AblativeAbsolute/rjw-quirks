﻿using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    public class SexAppraisalModifier_SetValue : SexAppraisalModifier
    {
        public float value;

        public override void ModifyValue(Pawn quirkOwner, Pawn partner, ref float value)
        {
            value = this.value;
        }

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (value <= 0f)
            {
                yield return "<value> must be > 0";
            }
        }
    }
}
