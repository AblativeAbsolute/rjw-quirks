﻿using rjw;
using rjwquirks.Modules.Shared.Events;

namespace rjwquirks.Modules.Quirks.Comps
{
    /// <summary>
    /// Base class for the comps that add thoughts on RJW event with SexProps argument
    /// </summary>
    public abstract class ThoughtAdder_OnSexEvent : ThoughtAdder
    {
        /// <summary>
        /// Check if thought should be applied
        /// </summary>
        public abstract bool ShouldApplyThought(SexProps props);

        protected override void HandleEvent(RjwEvent ev)
        {
            if (!ev.args.TryGetArg(RjwEventArgNames.SexProps, out SexProps props))
            {
                ModLog.Error($"{GetType()}.HandleEvent: No SexProps in the event");
                return;
            }

            if (ShouldApplyThought(props))
            {
                ApplyThought(props.pawn, props.partner);
            }
        }
    }
}
