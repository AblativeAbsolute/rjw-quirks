﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    /// <summary>
    /// Base class for the comps that add quirks to the pawns
    /// </summary>
    public abstract class Adder : QuirkComp
    {
        /// <summary>
        /// For def load only. Use <see cref="GetMessageFor"/>
        /// </summary>
        public string message;
        public MessageTypeDef messageType;

        protected string MessageTemplate => message ?? parent.description;

        public MessageTypeDef MessageType => messageType ?? MessageTypeDefOf.NeutralEvent;

        /// <summary>
        /// Get adjusted message text to be shown to the user when adding a quirk
        /// </summary>
        public string GetMessageFor(Pawn pawn) => MessageTemplate.Formatted(pawn.Named("pawn")).AdjustedFor(pawn).Resolve();

        /// <summary>
        /// Add quirk of comp parent def to the pawn
        /// </summary>
        protected void AddQuirkTo(Pawn pawn)
        {
            Quirk addedQuirk = pawn.GetQuirks().AddQuirk(parent);

            if (addedQuirk != null)
            {
                Messages.Message(GetMessageFor(pawn), pawn, MessageType);
            }
        }

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (eventDef == null)
            {
                yield return "<eventDef> is empty";
            }
        }
    }
}
