﻿using rjw.Modules.Interactions.Enums;
using rjw.Modules.Interactions.Objects;
using rjw.Modules.Shared;
using System.Collections.Generic;

namespace rjwquirks.Modules.Quirks.Comps
{
    public class PartKindUsageRules_ImpregnationFetish : PartKindUsageRules
	{
		/// <summary>
		/// Add desire to use penis if partner has vagina and vise-verse.
		/// Check of partner's parts is to avoid boosting vagina on futa when partner is female.
		/// No check of pawn's parts because interaction framework will filter it anyway
		/// </summary>
		public override IEnumerable<Weighted<LewdablePartKind>> GetModifiersForPawn(InteractionPawn quirkOwner, InteractionPawn partner)
		{
			if (!partner.BlockedParts.Contains(LewdablePartKind.Vagina))
			{
				yield return new Weighted<LewdablePartKind>(Multipliers.Doubled, LewdablePartKind.Penis);
			}
			if (!partner.BlockedParts.Contains(LewdablePartKind.Penis))
			{
				yield return new Weighted<LewdablePartKind>(Multipliers.Doubled, LewdablePartKind.Vagina);
			}
		}

		/// <summary>
		/// Ask partner to use penis if quirk owner has vagina and provide vagina if owner has penis.
		/// </summary>
		public override IEnumerable<Weighted<LewdablePartKind>> GetModifiersForPartner(InteractionPawn quirkOwner, InteractionPawn partner)
		{
			if (!quirkOwner.BlockedParts.Contains(LewdablePartKind.Vagina))
			{
				yield return new Weighted<LewdablePartKind>(Multipliers.Doubled, LewdablePartKind.Penis);
			}
			if (!quirkOwner.BlockedParts.Contains(LewdablePartKind.Penis))
			{
				yield return new Weighted<LewdablePartKind>(Multipliers.Doubled, LewdablePartKind.Vagina);
			}
		}
	}
}