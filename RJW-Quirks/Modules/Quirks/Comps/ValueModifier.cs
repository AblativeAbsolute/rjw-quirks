﻿using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    public abstract class ValueModifier : QuirkComp
    {
        public string valueName;
        public QuirkModifierPriority priority = QuirkModifierPriority.Normal;

        public void TryModifyValue(string valueName, ref float value)
        {
            if (this.valueName != valueName)
            {
                return;
            }

            ModifyValue(ref value);
        }

        public abstract void ModifyValue(ref float value);

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (valueName.NullOrEmpty())
            {
                yield return "<valueName> is empty";
            }
        }
    }
}
