﻿using rjw;
using rjwquirks.Modules.Shared.Events;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    /// <summary>
    /// Comp to remove an <see cref="hediff"/> to the pawn on RJW event
    /// </summary>
    public class HediffRemover : QuirkComp
    {
        public HediffDef hediff;

        protected override void HandleEvent(RjwEvent ev)
        {
            if (!ev.args.TryGetArg(RjwEventArgNames.Pawn, out Pawn pawn))
            {
                ModLog.Error($"{GetType()}.HandleEvent: No pawn in the event");
                return;
            }

            Hediff existingHediff = pawn.health?.hediffSet?.GetFirstHediffOfDef(hediff);
            if (existingHediff != null)
            {
                pawn.health.RemoveHediff(existingHediff);
            }
        }

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (hediff == null)
            {
                yield return "<hediff> is empty";
            }

            if (eventDef == null)
            {
                yield return "<eventDef> is empty";
            }
        }
    }
}
