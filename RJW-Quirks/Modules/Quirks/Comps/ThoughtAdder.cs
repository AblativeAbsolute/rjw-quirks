﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks.Comps
{
    /// <summary>
    /// Base class for the comps that add thoughts to the pawn
    /// </summary>
    public abstract class ThoughtAdder : QuirkComp
    {
        public ThoughtDef thought;

        /// <summary>
        /// Add <see cref="thought"/> to the <paramref name="pawn"/>
        /// </summary>
        public void ApplyThought(Pawn pawn, Pawn partner) => pawn.needs?.mood?.thoughts?.memories?.TryGainMemory(thought, partner);

        public override IEnumerable<string> ConfigErrors(QuirkDef parent)
        {
            foreach (string error in base.ConfigErrors(parent))
            {
                yield return error;
            }

            if (thought == null)
            {
                yield return "<thought> is empty";
            }

            if (eventDef == null)
            {
                yield return "<eventDef> is empty";
            }
        }
    }
}
