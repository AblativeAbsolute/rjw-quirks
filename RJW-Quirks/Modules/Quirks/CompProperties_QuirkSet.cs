﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace rjwquirks.Modules.Quirks
{
    public class CompProperties_QuirkSet : CompProperties
    {
        public CompProperties_QuirkSet()
        {
            compClass = typeof(QuirkSet);
        }
    }
}
