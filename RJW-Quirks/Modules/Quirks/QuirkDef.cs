﻿using RimWorld;
using rjw;
using rjwquirks.Modules.Quirks.Comps;
using rjwquirks.Modules.Quirks.SexSelectors;
using rjwquirks.Modules.Shared.PawnSelectors;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Quirks
{
    public class QuirkDef : Def
    {
        [MustTranslate]
        public string labelMale;
        [MustTranslate]
        public string labelFemale;
        public QuirkRarity rarity = QuirkRarity.Common;
        public bool hidden;
        public IPartnerSelector partnerPreference;
        public ISexSelector sexPreference;
        public List<OwnerRequirement> ownerRequirements = new List<OwnerRequirement>();
        public List<QuirkComp> comps = new List<QuirkComp>();
        public List<QuirkDef> conflictingQuirks = new List<QuirkDef>();
        public List<TraitDef> conflictingTraits = new List<TraitDef>();
        public List<string> exclusionTags = new List<string>();

        public string GetLabelFor(Gender gender)
        {
            if (gender == Gender.Male && !labelMale.NullOrEmpty())
            {
                return labelMale;
            }

            if (gender == Gender.Female && !labelFemale.NullOrEmpty())
            {
                return labelFemale;
            }

            return label;
        }

        public string GetLabelFor(Pawn pawn) => GetLabelFor(pawn?.gender ?? Gender.None);

        public IEnumerable<T> GetComps<T>() where T : QuirkComp
        {
            for (int i = 0; i < comps.Count; i++)
            {
                if (comps[i] is T compT)
                    yield return compT;
            }
        }

        public bool IsSatisfiedBySex(SexProps props) => sexPreference?.SexSatisfies(props) == true;

        public bool ConflictsWith(QuirkDef other)
        {
            if (other.conflictingQuirks?.Contains(this) == true || conflictingQuirks?.Contains(other) == true)
            {
                return true;
            }
            if (exclusionTags != null && other.exclusionTags != null)
            {
                for (int i = 0; i < exclusionTags.Count; i++)
                {
                    if (other.exclusionTags.Contains(exclusionTags[i]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public bool ConflictsWith(TraitDef traitDef)
        {
            if (/*traitDef.conflictingQuirks?.Contains(this) == true || */conflictingTraits?.Contains(traitDef) == true)
            {
                return true;
            }
            if (exclusionTags != null && traitDef.exclusionTags != null)
            {
                for (int i = 0; i < exclusionTags.Count; i++)
                {
                    if (traitDef.exclusionTags.Contains(exclusionTags[i]))
                    {
                        return true;
                    }
                }
            }
            return false;
        }

        public string GetDescriptionFor(Pawn pawn) => description.Formatted(pawn.Named("pawn")).AdjustedFor(pawn).Resolve();

        public override void PostLoad()
        {
            base.PostLoad();

            foreach (QuirkComp comp in comps)
            {
                comp.parent = this;
            }

            sexPreference?.SetParent(this);
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (partnerPreference != null)
            {
                foreach (string error in partnerPreference.ConfigErrors())
                {
                    yield return error;
                }
            }

            if (sexPreference != null)
            {
                foreach (string error in sexPreference.ConfigErrors())
                {
                    yield return error;
                }
            }

            foreach (OwnerRequirement req in ownerRequirements)
            {
                foreach (string error in req.ConfigErrors())
                {
                    yield return error;
                }
            }

            foreach (QuirkComp comp in comps)
            {
                foreach (string error in comp.ConfigErrors(this))
                {
                    yield return $"{comp.GetType()}: {error}";
                }
            }
        }
    }
}
