﻿namespace rjwquirks.Modules.Shared.Events
{
    public abstract class RjwEventHandler
    {
        public RjwEventHandlerDef def;
        public abstract void HandleEvent(RjwEvent ev);
    }
}
