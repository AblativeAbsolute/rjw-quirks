﻿namespace rjwquirks.Modules.Shared.Events
{
    public static class RjwEventArgNames
    {
        public static readonly string Pawn = "Pawn";
        public static readonly string SexProps = "SexProps";
        public static readonly string Quirk = "Quirk";
        public static readonly string Record = "Record";
        public static readonly string Satisfaction = "Satisfaction";
    }
}
