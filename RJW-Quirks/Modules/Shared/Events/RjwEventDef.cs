﻿using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Shared.Events
{
    public class RjwEventDef : Def
    {
        public List<string> obligatoryArgs;
    }
}
