﻿using System;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Shared.Events
{
    public class RjwEventHandlerDef : Def
    {
        public Type workerClass;
        public List<RjwEventDef> handlesEvents;

        private RjwEventHandler _worker;

        public RjwEventHandler Worker
        {
            get
            {
                if (_worker == null)
                {
                    _worker = (RjwEventHandler)Activator.CreateInstance(workerClass);
                    _worker.def = this;
                }
                return _worker;
            }
        }

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (workerClass == null)
            {
                yield return "<workerClass> is empty";
            }

            if (handlesEvents == null || handlesEvents.Count == 0)
            {
                yield return "<handlesEvents> is empty";
            }
        }
    }
}
