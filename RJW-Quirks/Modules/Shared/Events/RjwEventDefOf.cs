﻿using RimWorld;

namespace rjwquirks.Modules.Shared.Events
{
    [DefOf]
    public static class RjwEventDefOf
    {
        public static readonly RjwEventDef QuirkAddedTo;
        public static readonly RjwEventDef QuirkRemovedFrom;
        public static readonly RjwEventDef Orgasm;
        public static readonly RjwEventDef RecordChanged;
        public static readonly RjwEventDef PawnSexualized;
    }
}
