﻿using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    /// <summary>
    /// Partner selectors are similar to the pawn selectors, but can define relations between two pawns.
    /// </summary>
    public interface IPartnerSelector : IDefPart
	{
		/// <summary>
		/// Returns true if the partner satisfies all XML-defined conditions in relation to the pawn.
		/// Non-commutative
		/// </summary>
		bool PartnerSatisfies(Pawn pawn, Pawn partner);
	}
}