﻿using rjw;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class IsVisiblyPregnant : PawnSelector
    {
        public override bool PawnSatisfies(Pawn pawn) => pawn.IsVisiblyPregnant();
    }
}
