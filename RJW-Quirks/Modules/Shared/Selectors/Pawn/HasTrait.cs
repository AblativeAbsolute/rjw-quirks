﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class HasTrait : PawnSelector
    {
        public TraitDef trait;

        public override bool PawnSatisfies(Pawn pawn) => pawn.story?.traits?.HasTrait(trait) == true;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (trait == null)
            {
                yield return "<trait> is empty";
            }
        }
    }
}
