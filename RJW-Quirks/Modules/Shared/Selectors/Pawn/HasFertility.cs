﻿using rjw;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class HasFertility : PawnSelector
    {
        public override bool PawnSatisfies(Pawn pawn) => pawn.RaceHasFertility();
    }
}
