﻿using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class IsHumanlike : PawnSelector
    {
        public override bool PawnSatisfies(Pawn pawn) => pawn?.RaceProps?.Humanlike == true;
    }
}
