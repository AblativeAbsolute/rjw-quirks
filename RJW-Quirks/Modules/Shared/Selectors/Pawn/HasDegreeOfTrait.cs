﻿using RimWorld;
using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class HasDegreeOfTrait : PawnSelector
    {
        public TraitDef trait;
        public int degree = 0;

        public override bool PawnSatisfies(Pawn pawn) => pawn.story?.traits?.HasTrait(trait, degree) == true;

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (trait == null)
            {
                yield return "<trait> is empty";
            }
            else if (trait.degreeDatas.Find(d => d.degree == degree) == null)
            {
                yield return $"{trait.defName} has no data for a degree {degree}";
            }
        }
    }
}
