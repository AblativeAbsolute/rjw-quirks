﻿using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class LogicalNot : PawnSelector
    {
        public IPawnSelector negated;

        public override bool PawnSatisfies(Pawn pawn) => !negated.PawnSatisfies(pawn);

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (negated == null)
            {
                yield return "<negated> is empty";
            }
            else
            {
                foreach (string error in negated.ConfigErrors())
                {
                    yield return error;
                }

            }
        }
    }
}
