﻿using System.Collections.Generic;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public abstract class PawnSelector : IPawnSelector, IPartnerSelector
    {
        public abstract bool PawnSatisfies(Pawn pawn);

        public bool PartnerSatisfies(Pawn pawn, Pawn partner) => PawnSatisfies(partner);

        public virtual IEnumerable<string> ConfigErrors()
        {
            yield break;
        }
    }
}
