﻿using RimWorld;
using Verse;

namespace rjwquirks.Modules.Shared.PawnSelectors
{
    public class IsDisfigured : PawnSelector
    {
        public override bool PawnSatisfies(Pawn pawn) => RelationsUtility.IsDisfigured(pawn);
    }
}
