﻿using rjwquirks.Modules.Shared.PawnSelectors;
using System.Collections.Generic;

namespace rjwquirks.Modules.Shared.PartnerSelectors
{
    public abstract class LogicalMultipart : PartnerSelector
    {
        public List<IPartnerSelector> parts = new List<IPartnerSelector>();

        public override IEnumerable<string> ConfigErrors()
        {
            foreach (string error in base.ConfigErrors())
            {
                yield return error;
            }

            if (parts.Count < 2)
            {
                yield return "<parts> should have at least 2 elements";
            }

            foreach (var part in parts)
            {
                foreach (string error in part.ConfigErrors())
                {
                    yield return error;
                }
            }
        }
    }
}
