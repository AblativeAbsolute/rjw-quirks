﻿using HarmonyLib;
using HugsLib;
using Verse;

namespace rjwquirks
{
    public class Core : ModBase
    {
        // Originally did not use Hugslib, but due to RJW using hugslib I needed to in order for proper patching at correct times. could probably be done with priorities or something but this was easier
        /*public Core(ModContentPack pack)
        {
            //var harmony = new Harmony("rain.quirks");
            //harmony.PatchAll();
        }*/

        public override string ModIdentifier => "quirk";
    }
}
