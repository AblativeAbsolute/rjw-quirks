﻿using HarmonyLib;
using rjw;
using rjwquirks.Modules.Quirks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace rjwquirks.HarmonyPatches
{
    [HarmonyPatch(typeof(rjw.PawnExtensions), nameof(rjw.PawnExtensions.MaxEggSize))]
    public class Patch_PawnExtensions
    {
        [HarmonyPostfix]
        public static void AdjustMaxEggsSize(Pawn pawn, ref float __result)
        {
            if (pawn.HasQuirk(QuirkDefOf.Incubator))
            {
                __result *= 2f;
            }
        }
    }
}
