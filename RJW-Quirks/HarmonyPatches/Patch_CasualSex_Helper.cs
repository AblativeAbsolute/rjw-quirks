﻿using HarmonyLib;
using RimWorld;
using rjw;
using rjwquirks.Modules.Quirks;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Verse;

namespace rjwquirks.HarmonyPatches
{
    [HarmonyPatch(typeof(CasualSex_Helper), nameof(CasualSex_Helper.GetScore))]
    public class Patch_CasualSex_Helper
    {
        // Edits the "score" of a cell for a pawn to fuck in
        public static void Postfix(Pawn pawn, IntVec3 cell, Pawn partner, ref int __result)
        {
            QuirkSet quirks = pawn.GetQuirks();

            if (quirks.AllQuirks.EnumerableNullOrEmpty())
                return;

            List<Pawn> all_pawns = pawn.Map.mapPawns.AllPawnsSpawned.Where(x
                => x.Position.DistanceTo(pawn.Position) < 100
                && xxx.is_human(x)
                && x != pawn
                && x != partner
                ).ToList();

            // Somnophile code
            if (partner != null && quirks.Contains(QuirkDefOf.Somnophile))
            {
                if (all_pawns.Any(x
                    => !x.Awake()
                    && x.Position.DistanceTo(cell) < 6
                    && GenSight.LineOfSight(cell, x.Position, pawn.Map)
                    ))
                    __result += 50;
            }

            // Exhibitionist code
            if (quirks.Contains(QuirkDefOf.Exhibitionist))
            {
                bool might_be_seen = CasualSex_Helper.MightBeSeen(all_pawns, cell, pawn, partner);
                Room room = cell.GetRoom(pawn.Map);

                // Readd the 30 score removed in regular RJW
                __result += 30;
                // Readd the 100 score taken from being in a doorway in regular RJW
                if (room.IsDoorway) __result += 100;

                if (might_be_seen)
                    __result += 35;
                else
                    __result -= 10;

                if (room.Role == RoomRoleDefOf.Barracks || room.Role == RoomRoleDefOf.PrisonBarracks || room.Role == RoomRoleDefOf.PrisonCell
                    || room.Role == RoomRoleDefOf.Laboratory || room.Role == RoomRoleDefOf.RecRoom
                    || room.Role == RoomRoleDefOf.DiningRoom || room.Role == RoomRoleDefOf.Hospital
                    )
                    __result += 15;     // Add 15 instead of 10 to counteract the -5 in regular RJW

                Dictionary<int, int> cell_doors = new Dictionary<int, int>();

                var doors = cell_doors.TryGetValue(room.ID);

                if (doors > 1)
                    __result += 7 * doors;          // Multiply by 7 instead of 2 to counteract the negative in regular RJW
            }
        }
    }
}
